from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from poll.models import Choice, Question
from django.views import generic

#function based view
#every function works with requests and http response 
#def index(request):
#    latest_question_list = Question.objects.all()
#    return render(request, "poll/index.html", {
#        "latest_question_list": latest_question_list
#    })

#def detail(request, question_id):
#    """details of every question"""
#    question = get_object_or_404(Question, pk=question_id)
#    return render(request, 'poll/detail.html', {'question': question})


#def results(request, question_id):
#    """details of every question result"""
#    question = get_object_or_404(Question, pk=question_id)
#    return render(request, 'poll/results.html', {'question': question})


class IndexView(generic.ListView):
    template_name = 'poll/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Question
    template_name = 'poll/detail.html'


class ResultView(generic.DetailView):
    model = Question
    template_name = 'poll/results.html'


def vote(request, question_id):
    """Functions for voting confirmation"""
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
        pass
    except(KeyError, Choice.DoesNotExist):
        return render(request, 'poll/detail.html', {
            'question': question,
            'error_message': "Tu no elegiste ninguna opción",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('poll:results', args=(question.id,)))
