from django.urls import path

from . import views #from here import...

app_name = 'poll'
urlpatterns = [
    #ex: /polls/
    path("", views.IndexView.as_view, name="index"),
    #ex: /polls/5/
    #lo que esta en question id es lo que se pasara por parametro a la funcion
    #ex: /polls/5/details
    path("<int:pk>/", views.DetailView.as_view, name="detail"),
    #ex: /polls/5/results
    path("<int:pk>/results/", views.ResultView.as_view, name="results"),
    #ex: /polls/5/vote
    path("<int:question_id>/vote/", views.vote, name="vote"),
]

# "< >/ is the way we pass parameters and variables through the url we are setting"